

module.exports = function(config) {

	var MongoClient = require('mongodb').MongoClient
    , format = require('util').format;

	var module = {};

	MongoClient.connect(config.mongodb, function(err, db) {
    	if(err) throw err;

    	var collection = db.collection('smartmeter');
    	collection.insert({a:2}, function(err, docs) {

      		collection.count(function(err, count) {
        		console.log(format("count = %s", count));
      		});

      		// Locate all the entries using find
      		collection.find().toArray(function(err, results) {
        		console.dir(results);
        		// Let's close the db
        		db.close();
      		});
    	});
  	});

	module.write = function(measurement) {

	};

	return module;
};