
var config = require('./config.json');

var reader = require('./meterreader.js')(config.reader);
var parser = require('./meterparser.js')(config.parser);
var writer = require('./meterwriter-' + config.writer.type + '.js')(config.writer);

writer.setup(function() {
	reader.onTelegram(parser.parse);
	parser.onMeasurement(writer.write);
});
