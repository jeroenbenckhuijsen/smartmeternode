
/*
ser.timeout=20
*/

module.exports = function(config) {

	var events = require('events');
	var cleanup = require('./cleanup.js');
	var serialport = require("serialport");

	/*

	FROM: http://gathering.tweakers.net/forum/list_messages/1629675
	See also: http://domoticx.com/p1-poort-slimme-meter-hardware/

	 Merk:   Model:  Type:   Uitvoering P1 poort:    DSMR versie:    Seriële instellingen:   Geïnverteerd signaal?   P1 telegram header:
	 Iskra   ME 382  1-fase kleinverbruik    RJ-11, 6-pins   2.2 				 9600 7E1    Ja  /ISk5\
	 Iskra   MT 382  3-fase kleinverbruik    RJ-11, 6-pins   2.2 				 9600 7E1    Ja  /ISk5\
	 Kaifa   MA105   1-fase kleinverbruik    RJ-11, 6-pins   4.0 (4.0.5 / 4.0.7) 115200 8N1  Ja  /KFM5
	 Kaifa   MA105C  1-fase kleinverbruik    RJ-11, 6-pins   4.2.2               115200 8N1? Ja? /KFM5
	 Kaifa   MA304   3-fase kleinverbruik    RJ-11, 6-pins   4.0 (4.0.5 / 4.0.7) 115200 8N1  Ja  /KFM5
	 Kaifa   MA304C  3-fase kleinverbruik    RJ-11, 6-pins   4.2.2               115200 8N1? Ja? /KFM5
	 Kamstrup    162 1-fase kleinverbruik    RJ-11, 4-pins   2.2 				 9600 7E1    Ja  /KMP5
	 Kamstrup    351 3-fase (ext. stroom spoelen)    RJ-11, 6-pins   2.2 		 9600 7E1    Ja  /KMP5
	 Kamstrup    382 3-fase kleinverbruik    RJ-11, 4-pins   2.2 				 9600 7E1    Ja  /KMP5
	 Landis + Gyr    E350 (ZCF100)   1-fase kleinverbruik    RJ-11, 6-pins   4.0 115200 8N1  Ja? /XMX5LG
	 Landis + Gyr    E350 (ZFF100)   3-fase (3-aderig) kleinverbruik RJ-11, 6-pins   4.0 115200 8N1  Ja? /XMX5LG
	 Landis + Gyr    E350 (ZMF100)   1-fase / 3-fase (4-aderig) kleinverbruik    RJ-11, 6-pins   4.0 115200 8N1  Ja? /XMX5LG

	 */
	var p1Types = {
		"iskra" : {
			"com_setup" : {
				baudrate: 9600,
				databits: 7,
				stopbits: 1,
				parity: 'even'
			},
			"telegramStart" : "/ISk5\\",
			"telegramEnd" : "!"
		},
		"kaifa" : {
			"com_setup" : {
				baudrate: 115200,
				databits: 8,
				stopbits: 1,
				parity: 'none'
			},
			"telegramStart" : "/KFM5KAIFA-METER",
			"telegramEnd" : "!"
		},
		"kamstrup" : {
			"com_setup" : {
				baudrate: 9600,
				databits: 7,
				stopbits: 1,
				parity: 'even'
			},
			"telegramStart" : "/KMP5",
			"telegramEnd" : "!"
		},
		"landis" : {
			"com_setup" : {
				baudrate: 115200,
				databits: 8,
				stopbits: 1,
				parity: 'none'
			},
			"telegramStart" : "/XMX5LG",
			"telegramEnd" : "!"
		}
	};

	p1Types.iskra_me382 = p1Types.iskra;
	p1Types.iskra_mt382 = p1Types.iskra;

	p1Types.kaifa_ma105 = p1Types.kaifa;
	p1Types.kaifa_ma105c = p1Types.kaifa;
	p1Types.kaifa_ma304 = p1Types.kaifa;
	p1Types.kaifa_ma304c = p1Types.kaifa;

	p1Types.kamstrup_162 = p1Types.kamstrup;
	p1Types.kamstrup_351 = p1Types.kamstrup;
	p1Types.kamstrup_382 = p1Types.kamstrup;

	p1Types.landis_zcf100 = p1Types.landis;
	p1Types.landis_zff100 = p1Types.landis;
	p1Types.landis_zmf100 = p1Types.landis;


	var com_defaults = {
		parser: serialport.parsers.readline("\n"),
		xon: 0,
		xoff: 0,
		rtscts: 0
	};

	function copyProperties(source, target) {
		for (var key in source) {
			target[key] = source[key];
		}
	}

	var module = {};

	var eventEmitter = new events.EventEmitter();
	var p1Config = p1Types[config.metertype];

	console.log('Connecting to smart meter ' + config.metertype + ' on TTY ' + config.tty);

	var com_setup = {};
	copyProperties(com_defaults, com_setup);
	copyProperties(p1Config.com_setup, com_setup);

	var SerialPort = serialport.SerialPort;
	var serialPort = new SerialPort(config.tty, com_setup);

	serialPort.on("open", function () {
		console.log('Serial connection to Smart meter established');

		var telegram = [];

		var telegramStarted = false;

  		serialPort.on('data', function(data) {
  			if (data.indexOf(p1Config.telegramStart) == 0) {
  				telegramStarted = true;
  				console.log("Smart meter telegram started")
  			} else if (data.indexOf(p1Config.telegramEnd) == 0) {
  				console.log("Smart meter telegram received:\n" + telegram.join("\n"));
  				module.emitter.emit("telegram", telegram);

  				telegramStarted = false;
  				telegram = [];
  			} else if (telegramStarted) {
  				telegram.push(data);
  			} else {
  				console.log("Unknown smart meter data discarded: " + data);
  			}
  		});
	});

	cleanup.onExit(function(options, err) {
		if (err) {
			console.log("Error: " + err)
		}
		console.log("Closing serial connection");
		if (serialPort.isOpen()) {
			serialPort.close();
		}
	});

	module.port = serialPort;

	module.emitter = eventEmitter;

	module.onTelegram = function (callback) {
		module.emitter.on("telegram", callback);
		return module;
	};

	return module;
};

/*

Smart meter data received: /KFM5KAIFA-METER


Smart meter data received: 1-3:0.2.8(42)

Smart meter data received: 0-0:1.0.0
Smart meter data received: (150909164719S)

Smart meter data received: 0
Smart meter data received: -0:96.1.1(453030323530303030333433
Smart meter data received: 3134323135)

Smart meter data received: 1
Smart meter data received: -0:1.8.1(000103.731*kWh)

Smart meter data received: 1-0:
Smart meter data received: 1.8.2(000067.40
Smart meter data received: 0*kWh)

Smart meter data received: 1-0:
Smart meter data received: 2.8.1(000002.97
Smart meter data received: 8*kWh)

Smart meter data received: 1-0:
Smart meter data received: 2.8.2(000007.832*kWh)

Smart meter data received: 0-0:96.14.0(0
Smart meter data received: 002)

Smart meter data received: 1-0:1.
Smart meter data received: 7.0(00.141*kW)

Smart meter data received: 1
Smart meter data received: -0:2.7.0(00.000*
Smart meter data received: kW)

Smart meter data received: 0
Smart meter data received: -0:96.7.21(00011
Smart meter data received: )

Smart meter data received: 0
Smart meter data received: -0:96.7.9(00007)

Smart meter data received: 1-0:9
Smart meter data received: 9.97.0(1)(0-0:96.7.19)
Smart meter data received: (0
Smart meter data received: 00101000001W)(2147483647*s)
Smart meter data received: 

Smart meter data received: 1-0:32.32.0(00
Smart meter data received: 000)

Smart meter data received: 1-0:32.36.0(000
Smart meter data received: 00)

Smart meter data received: 0
Smart meter data received: -0:96.13.1(
Smart meter data received: )

Smart meter data received: 0-
Smart meter data received: 0:96.13.0(
Smart meter data received: )

Smart meter data received: 1-0:31.7.0(00
Smart meter data received: 1*A)

Smart meter data received: 1-0
Smart meter data received: :21.7.0(00.150*kW)

Smart meter data received: 1-0
Smart meter data received: :22.7.0(00.000*kW)

Smart meter data received: 0-1:
Smart meter data received: 24.1.0(003)

Smart meter data received: 0
Smart meter data received: -1:96.1.0(47303032353030333236303
Smart meter data received: 63438323135)
Smart meter data received: 

Smart meter data received: 0-1:2
Smart meter data received: 4.2.1(150909160000S)(00007.336*m3)
Smart meter data received: 
!5E72

*/