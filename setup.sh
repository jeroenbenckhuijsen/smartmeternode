#/bin/sh
#
# Setup script for SmartMeterNode
# Execute with sudo

apt-get install nodejs
npm install forever -g

BASE_DIR=`pwd`
LOG_DIR=/var/log/smart-meter-node
PID_DIR=/var/run/smart-meter-node

mkdir $LOG_DIR
mkdir $PID_DIR

# TODO: Log files niet op raspberry pi schijf!!

cat << INITD > /etc/init.d/smart-meter-node
### BEGIN INIT INFO
# Provides:             smart-meter-node
# Required-Start:
# Required-Stop:
# Default-Start:        2 3 4 5
# Default-Stop:         0 1 6
# Short-Description:    Smart Meter Node App
### END INIT INFO

NODE_PREFIX=/usr/local/

export PATH=\$PATH:\$NODE_PREFIX/bin
export NODE_PATH=\$NODE_PATH:\$NODE_PREFIX/lib/node_modules

case "\$1" in
  start)
    \$NODE_PREFIX/bin/forever start -e $LOG_DIR/error.log -l $LOG_DIR/forever.log -o $LOG_DIR/output.log --sourceDir="$BASE_DIR" index.js
    ;;
  stop)
    exec \$NODE_PREFIX/bin/forever stopall
    ;;
  *)

  echo "Usage: /etc/init.d/smart-meter-node {start|stop}"
  exit 1
  ;;
esac
exit 0
INITD

chmod 755 /etc/init.d/smart-meter-node
update-rc.d smart-meter-node defaults
