

module.exports = function(config) {
	var module = {};

	var events = require('events');
	var eventEmitter = new events.EventEmitter();

	module.emitter = eventEmitter;

	var p1ParserConfig = {
		"1-0:1.8.1" : "offpeakDelivered",
		"1-0:1.8.2" : "peakDelivered",
		"1-0:2.8.1" : "offpeakDeliveredBack",
		"1-0:2.8.2" : "peakDeliveredBack",
		"1-0:1.7.0" : "currentUsage",
		"1-0:2.7.0" : "currentBackDelivery"
	};

	module.parse = function(telegram) {

		var measurement = {
			date : Date.now()
		};

		for (var i = 0; i < telegram.length; i++) {
			var line = telegram[i];

			for (var key in p1ParserConfig) {
				if (line.indexOf(key) == 0) {
					var valStart = line.indexOf("(") + 1;
					var valEnd = line.indexOf("*", valStart);
					var value = line.substring(valStart, valEnd);

					measurement[p1ParserConfig[key]] = value;
				}
			}
		}

		console.log(measurement);

		module.emitter.emit("measurement", measurement);

/*
1-3:0.2.8(42)
0-0:1.0.0(150912221726S)
0-0:96.1.1(4530303235303030303334333134323135)
1-0:1.8.1(000128.006*kWh)     	// +T1
1-0:1.8.2(000077.342*kWh)		// +T2
1-0:2.8.1(000003.710*kWh)		// -T1
1-0:2.8.2(000017.362*kWh)		// -T2
0-0:96.14.0(0001)				// Actuele tarief
1-0:1.7.0(00.633*kW)			// Huidig verbruik
1-0:2.7.0(00.000*kW)			// Huidige teruglevering
0-0:96.7.21(00011)
0-0:96.7.9(00007)
1-0:99.97.0(1)(0-0:96.7.19)(000101000001W)(2147483647*s)
1-0:32.32.0(00000)
1-0:32.36.0(00000)
0-0:96.13.1()
0-0:96.13.0()
1-0:31.7.0(002*A)
1-0:21.7.0(00.633*kW)
1-0:22.7.0(00.000*kW)
0-1:24.1.0(003)
0-1:96.1.0(4730303235303033323630363438323135)
0-1:24.2.1(150912220000S)(00012.586*m3)



0-0:96.1.1(204B413655303031363630323937393132)                                                                          
1-0:1.8.1(00013.000*kWh)                                                        +T1 = 13     Afgenomen stroom daltarief 13 kWh
8.2(00001.000*kWh)                                                              +T2 = 1      Afgenomen stroom piektarief 1 kWh      
1-0:2.8.1(00026.000*kWh)                                                        -T1 = 26     Teruggeleverde stroom daltarief 26 kWh
1-0:2.8.2(00000.000*kWh)                                                        -T2 = 0      Teruggeleverde stroom piektarief 0 kWh
0-0:96.14.0(0001)                                                               Aktuele tarief (1) dus daltief
1-0:1.7.0(0000.54*kW)                                                           Huidig verbruik (afgenomen vermogen) 540 Watt
1-0:2.7.0(0000.00*kW)                                                           Huidige teruglevering 0 Watt
0-0:17.0.0(999*A)                                                               Maximum stroom/fase
0-0:96.3.10(1)                                                                  stand van de schakelaar
0-0:96.13.1()                                                                   bericht numeriek
0-0:96.13.0()                                                                   bericht tekst
0-1:24.1.0(3)                                                                   andere apparaten op de M-Bus
0-1:96.1.0(3238313031353431303034303232323131)                                  identificatie van de gasmeter
0-1:24.3.0(121028200000)(00)(60)(1)(0-1:24.2.1)(m3)                             tijd van de laatste gasmeting (121028200000) 2012-10-28
(00015.475)                                                                     verbruikte hoeveelheid 15 m3 gas
0-1:24.4.0(1)                                                                   stand gasklep
*/		
	};

	module.onMeasurement = function(callback) {
		module.emitter.on("measurement", callback);
		return module;
	};

	return module;
};
