

module.exports = function(config) {

	var nano = require('nano')(config.couch.url)
    , format = require('util').format;

	var module = {};

  module.setup = function(callback) {
    nano.db.create('p1meter', function(err, body) {
      if (err == null || err.statusCode == 412) { // 412 == file_exists
        module.db = nano.db.use('p1meter');
        console.log("Connected to couch db");
        callback();
      } else {
        throw err;
      }
    });
  };

	module.write = function(measurement) {
      var docname = "" + Date.now();
      console.log("Received measurement, inserting into DB with id " + docname);
      module.db.insert(measurement, docname, function(err, body) {
        if (err) {
          console.log("Inserting of document failed:" + err + "\n. Document is " + measurement);
        } else {
          console.log("Inserted document with id " + docname)
        }
      })
	};

	return module;
};
